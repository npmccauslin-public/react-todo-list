﻿const bodyParser = require("body-parser");
const express = require("express");
const mysql = require("mysql");
const path = require('path')

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, '../build')));

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'todolist',
    timezone: "+00:00"
});

connection.connect(function(err) {
    if (!!err) {
        console.log(`Cannot connect to database.`);
        console.log(err);
    }
    else {
        console.log(`Connected to database!`);
    }
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../build', 'index.html'))
})

app.get('/api/users', function(req, res) {
    connection.query('SELECT * FROM users', function(err, rows, fields) {
        if (!!err) {
            console.log(err)
        }
        else {
            return res.send(rows);
        }
    });
});

app.get('/api/user-list', function(req, res) {
    const username = req.query.username;
    
    connection.query(`SELECT * FROM todolists WHERE user = '${username}'`, function(err, rows, fields) {
        if (!!err) {
            console.log(err)
        }
        else {
            return res.send(rows);
        }
    });
});
app.post('/api/delete-list', function(req, res) {
    const username = req.body.name;
    const listName = req.body.listName;

    connection.query(`DELETE FROM todolists WHERE user = '${username}' AND name = '${listName}'`, function(err, rows, fields) {
        if (!!err) {
            console.log(err)
        }
        else {
            return res.send(rows);
        }
    });
});
app.post('/api/update-list', function(req, res) {
    const name = req.body.name;
    const list = req.body.list;
    const listName = req.body.listName;
    connection.query(`UPDATE todolists SET list = '${list}' where user = '${name}' AND name = '${listName}';`,
        (err, result) => {
            if (err) {
                console.log(err)
            }
        }
    );
    res.sendStatus(200);
});
app.post('/api/add-list', function(req, res) {
    const name = req.body.name;
    const listName = req.body.listName;
    connection.query("INSERT INTO todolists (user, name, list) VALUES (?, ?, ?)",
        [name, listName, '[]'],
        (err, result) => {
            if (err) {
                console.log(err);
                res.send({message: "Unable to create list."})
                return -1;
            }
            else {
                res.sendStatus(200);
            }
        });
});

const port = process.env.PORT || 3001;
app.listen(port, () => {
    console.log("Server Connected to " + port);
})