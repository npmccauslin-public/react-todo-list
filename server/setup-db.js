﻿const mysql = require("mysql");

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: ''
});

connection.query('CREATE DATABASE IF NOT EXISTS todolist', function(err, rows) {
    if (!!err) {
        console.log(err)
    }
    else {
        connection.query('USE todolist;', function (error, results) {
            if (error) {
                throw error
            }
            const todolistTableQuery = `
                CREATE TABLE IF NOT EXISTS todolists(
                    user varchar(15) NOT NULL,
                    list longtext NOT NULL,
                    name varchar(15) NOT NULL DEFAULT 'MyList'
                )`;
            const userTableQuery = `
                CREATE TABLE IF NOT EXISTS users(
                    username varchar(15) NOT NULL,
                    creationDate datetime DEFAULT current_timestamp()
                )`;
            connection.query(todolistTableQuery, function(err, rows) {
                if (!!err) {
                    console.log(err)
                }
            });
            connection.query(userTableQuery, function(err, rows) {
                if (!!err) {
                    console.log(err)
                }
            });
            console.log(`Database setup complete.`);
            connection.end();
        });
    }
});