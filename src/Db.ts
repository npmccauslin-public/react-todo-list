﻿import axios from "axios";
import {TodoList} from "./components/Lists";

export function getUser(username: string) {
    return axios
        .get("/api/users")
        .then(result => {
            const res = result.data;
            if (res) {
                return username;
            }
            alert('Could not set user, user does not exist.');
        })
}
export function getListsForUser(username: string) {
    return axios
        .get("/api/user-list", {params: {'username': username}})
        .then(result => {
            const res = result.data;
            if (res) {
                return res;
            }
            alert('Could not get todo-list for user.');
        })
}
export function setListInDb(username:string, list: TodoList | undefined){
    if (!list) {
        throw new Error("Cannot set list in database.");
    }
    return axios.post(
        "/api/update-list",
        {
            name: username,
            listName: list.name,
            list: JSON.stringify(list.list)
        }
    )
}
export function removeListInDb(username:string, listName: string | undefined){
    if (!listName) {
        throw new Error("Cannot delete list in database.");
    }
    return axios.post(
        "/api/delete-list",
        { 
            name: username,
            listName: listName,
        }
    )
}
export function addListInDb(username:string, listName: string){
    return axios.post(
        "/api/add-list",
        {
            name: username,
            listName: listName
        }
    )
}