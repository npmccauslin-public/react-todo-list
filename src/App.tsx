import React from "react";
import {Lists, TodoList} from "./components/Lists";
import "./master.scss";
import {getListsForUser, getUser} from "./Db";

function App() {
  const [username, setUsername] = React.useState("");
  const [lists, setLists] = React.useState<TodoList[]>([]);
  const [isLoading, setIsLoading] = React.useState(true);
  React.useEffect(() => {
      // Hardcode admin login as we don't need to allow other users at this time
      getUser("admin")
          .then(res => setUsername(res ?? "admin"))
          .then(() => getListsForUser("admin"))
          .then(res => {
              const lists: TodoList[] = [];
              res.forEach((r: { name: string; list: string; }) => {
                  const listed: string = r.list.replaceAll('\\"', "");
                  lists.push({name:r.name, list: JSON.parse(listed) ?? []})
              })
              setLists(lists);
          })
          .then(() => setIsLoading(false));
  }, [])
    
  const getContent = () => {
      return !isLoading 
                ? <Lists user={username} lists={lists}/>
                : <h1>Loading...</h1>
  }
  return ( 
    <div className="App">
        {getContent()}
    </div> 
  );
} 
  
export default App;
