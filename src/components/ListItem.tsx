﻿import {Col, Row} from "react-bootstrap";
import {XLg} from "react-bootstrap-icons";
import * as React from "react";

export interface TodoItem {
    id: string;
    text: string;
    isChecked?: boolean;
}
interface Props {
    item: TodoItem;
    onToggle: (itemId: string) => void;
    onRemove: (itemId: string) => void;
}
export const ListItem = (props: Props) => {
    return <Row>
        <Col md={11} sm={11} xl={11} className={"check-col"}>
            <span
                className={`list-item ${props.item.isChecked ? "checked" : ""}`}
                >
                <input
                    className={"check-item"}
                    type={"checkbox"}
                    id={props.item.id}
                    onClick={() => {
                        props.onToggle(props.item.id);
                    }}
                />
                <span className={`list-item ${props.item.isChecked ? "checked" : ""}`}>{props.item.text}</span>
        </span>
        </Col>
        <Col md={1} sm={1} xl={1} className={"remove-col"}>
            <XLg className={"remove-item"} onClick={() => props.onRemove(props.item.id)}/>
        </Col>
    </Row>
}
