﻿import {Button, Col, Container, Row} from "react-bootstrap";
import * as React from "react";
import {PlusLg, XLg} from "react-bootstrap-icons";
import {ListItem, TodoItem} from "./ListItem";
import {addItem, addList, getListByName, removeItem, removeList, toggleItem} from "../model/ListModel";
import {addListInDb, removeListInDb, setListInDb} from "../Db";

export interface TodoList {
    name: string;
    list: TodoItem[];
}
interface Props {
    user: string;
    lists: TodoList[];
}
export const Lists = (props: Props) => {
    const [todoLists, setTodoLists] = React.useState<TodoList[]>(props.lists);
    const [addInput, setAddInput] = React.useState("");
    const [newListName, setNewListName] = React.useState("");
    const [currentList, setCurrentList] = React.useState<TodoList | undefined>(props.lists[0]);
    
    React.useEffect(() => {
        let newLists = [...todoLists];
        if (currentList && !getListByName(newLists, currentList?.name)) {
            newLists = addList(newLists, currentList); 
        }
        else {
            newLists = newLists.map(list => {
                return list.name === currentList?.name
                    ? currentList
                    : list;
            });
        }
        setTodoLists(newLists);
    }, [currentList]);
    
    const addListItem = (listName: string | undefined, itemId: string, text: string): TodoList[] => {
        if (listName === undefined) {
            throw new Error("Cannot add item to list. Invalid list name provided.");
        }
        const copyMainList = [...todoLists];
        const index = todoLists.findIndex((list) => list.name === listName);
        copyMainList[index] = addItem(copyMainList[index], {id: itemId, text: text});
        return copyMainList;
    }
    const removeListItem = (itemId: string): void => {
        const newList = removeItem(currentList, itemId);
        setListInDb(props.user, newList)
            .then(() => setCurrentList(newList))
    }
    const toggleChecked = (itemId: string): void => {
        todoLists.map(list => {
            const newList = toggleItem(list, itemId);
            return list.name === currentList?.name
                ? setListInDb(props.user, newList)
                    .then(() => setCurrentList(newList))
                : list;
        });
    }
    const getCompletedItems = (): JSX.Element[] | undefined => {
        return currentList?.list
                .filter(item => item.isChecked)
                .map(item =>
                    <ListItem
                        key={item.id}
                        item={item}
                        onToggle={toggleChecked}
                        onRemove={removeListItem}
                    />
                );
    }
    const getIncompleteItems = (): JSX.Element[] | undefined => {
        return currentList?.list
            .filter(item => !item.isChecked)
            .map(item =>
                <ListItem
                    key={item.id}
                    item={item}
                    onToggle={toggleChecked}
                    onRemove={removeListItem}
                />
            ); 
    }
    const getCheckedView = () => {
        const checkedItems: JSX.Element[] | undefined = getCompletedItems();
        return checkedItems && checkedItems.length > 0 &&
            <div>
                <hr />
                <h4 className={"complete-header"}>
                    <span>Completed Items</span>
                </h4>
                <br/><br/>
                {getCompletedItems()}
            </div>;
    }
    const handleListAdd = () => {
        addListInDb(props.user, newListName)
            .then(() => setCurrentList(  {name: newListName, list: []}));
    }
    const handleItemAdd = () => {
        if (addInput.length > 0) {
            const newLists = addListItem(
                currentList?.name,
                `list-item-${getListByName(todoLists, currentList?.name)?.list.length}`,
                addInput
            );
            setListInDb(props.user, getListByName(newLists, currentList?.name))
                .then(() => setCurrentList(getListByName(newLists, currentList?.name)));
            setAddInput("");
        }
    } 
    const getListGridView = () => {
        return <div className={"button-grid"}>
            {todoLists.map((list, i) => {
                const key = `list-button ${list.name === currentList?.name ? 'active' : ''}`;
                return <Button
                    key={key + "_" + i}
                    className={key}
                    onClick={() => setCurrentList(getListByName(todoLists, list.name))}
                >
                    {list.name}
                </Button>
            })}
            <span>
                        <input
                            className={"list"}
                            placeholder={"New list name"}
                            onChange={(e) => setNewListName(e.target.value)}
                            onKeyPress={(e) => {
                                if (e.key === "Enter") {
                                    handleListAdd();
                                }
                            }}
                        />
                        <PlusLg
                            className={"add-list-icon"}
                            onClick={handleListAdd}
                        />
                    </span>
        </div>
    }
    const getCurrentListHeaderView = () => {
        return currentList
                ? <h2 className={"current-list-header"}>
                    <span>{currentList.name}</span>
                    <XLg
                        className={"remove-item"}
                        onClick={() => {
                            const newLists = removeList(todoLists, currentList?.name);
                            removeListInDb(props.user, currentList?.name)
                                .then(() => {
                                    setTodoLists(newLists);
                                    setCurrentList(newLists.length > 0 ? newLists[0] : undefined);
                                })
                        }}
                    />
                </h2>
                : <h2>Create a new todo list on the left!</h2> 
    }
    const getIncompleteHeaderView = (): JSX.Element => {
        return <h4>
            <span>
                {
                    currentList && getIncompleteItems()?.length
                        ? "Incomplete Items"
                        : "Create todo items below"
                }
            </span>
        </h4>
    }
    const getCurrentListView = () => {
        return currentList?.name &&
            <div>
                {getIncompleteHeaderView()}
                <br/><br/>
                {getIncompleteItems()}
                <span className={"unchecked-span"}>
                        <input
                            placeholder={"Add Item"}
                            onChange={(e) => setAddInput(e.target.value)}
                            value={addInput}
                            onKeyPress={(e) => {
                                if (e.key === "Enter") {
                                    handleItemAdd();
                                }
                            }}
                        />
                        
                        <PlusLg
                            className={"add-item-icon"}
                            onClick={handleItemAdd}
                        />
                    </span>
                <br/>
                {getCheckedView()}
            </div>
    }
    return <Container>
        <h1>{props.user}'s Todo Lists</h1>
        <Row>
            <Col md={"4"} sm={"4"} xs={"4"}>
                {todoLists.length > 0 && <h2>Your Lists</h2>}
                {getListGridView()}
            </Col>
            <Col md={"8"} sm={"8"} xs={"8"} className={"current-list-col"}>
                {getCurrentListHeaderView()}
                <br/>
                {getCurrentListView()}
            </Col>
        </Row>
    </Container> 
}