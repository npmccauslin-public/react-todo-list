﻿import {addItem, addList, removeItem, removeList, toggleItem} from "./ListModel";
import {TodoList} from "../components/Lists";

describe("List Model", () => {
    const initialLists: TodoList[] = [];
    const initialList: TodoList = {
        name: "myList",
        list: []
    }
    it ("adds items to list", () => {
        const numItems = 10;
        expect(
            addItemsToList(initialList, numItems).list.length).toEqual(numItems + initialList.list.length
        );
    })
    it ("removes item from list", () => {
        const numItems = 10;
        const newList = addItemsToList(initialList, numItems);
        expect(
            removeItem(newList, "item4").list
                .filter(item => item.id === "item4")
                .length
        ).toEqual(0);
    });
    it ("toggles list item", () => {
        const numItems = 10;
        const newList = addItemsToList(initialList, numItems);
        expect(
            toggleItem(newList, "item5").list
                .find(item => item.id === "item5")
                ?.isChecked
        ).toBeTruthy();
    })
    it("adds list", () => {
        expect(
            addList(initialLists, {name: "list2", list: []}).length
        ).toEqual(1);
    })
    it("removes list", () => {
        const newList = addList(initialLists, {name: "list2", list: []});
        expect(
            removeList(newList, "list2").length
        ).toEqual(0);
    })
    const addItemsToList = (list: TodoList, itemNum: number): TodoList => {
        let newList = {...list};
        for (let i = 0; i < itemNum; i++) {
            newList = addItem(newList, {id: "item" + i, text: "testing2"});
        }
        return newList;
    }
})