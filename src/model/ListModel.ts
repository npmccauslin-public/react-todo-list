﻿import {TodoItem} from "../components/ListItem";
import {TodoList} from "../components/Lists";

export function addItem(list: TodoList, item: TodoItem): TodoList {
    return {
        ...list,
        list: [...list.list, item]
    };
}
export function removeItem(list: TodoList | undefined, itemId: string): TodoList {
    if (!list) {
        throw new Error("Cannot remove item.");
    }
    const newList: TodoItem[] = list.list.filter(item => item.id !== itemId); 
    return {
        ...list,
        list: newList
    }
}
export function toggleItem(list: TodoList | undefined, itemId: string): TodoList {
    if (!list) {
        throw new Error("Cannot toggle item.");
    }
    const newList = list.list.map(item => {
        if (item.id === itemId) {
            return {...item, isChecked: !item.isChecked};
        }
        return item;
    });
    return {
        name: list.name,
        list: newList
    }
}
export function addList(lists: TodoList[], list: TodoList | undefined): TodoList[] {
    if (!list) {
        throw new Error("Cannot toggle item.");
    }
    return [...lists, list];
}
export function removeList(lists: TodoList[], listName: string | undefined): TodoList[] {
    if (!listName) {
        throw new Error("Cannot delete list.");
    }
    return lists.filter(list => list.name !== listName);
}
export function getListByName(lists: TodoList[], name:string | undefined): TodoList | undefined {
    if (!name) {
        throw new Error("Cannot get list. Invalid list name provided.");
    }
    return lists.find(list => list.name === name);
}
export function contain(lists: TodoList[], name:string | undefined): TodoList | undefined {
    if (!name) {
        throw new Error("Cannot get list. Invalid list name provided.");
    }
    return lists.find(list => list.name === name);
}