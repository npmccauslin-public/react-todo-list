# React Todo List Application
## Features
* Create new lists
* Delete existing lists
* Add list items
* Check and uncheck items
* Remove existing items from list

* Separation of styling from elements
* Scss css extended syntax 
* Express for rest api
* Nodejs connection to mysql
* Functional React with hooks
* Typescript
* Unit testing with Jest
* ES6 syntax

## Running application
Run the command `yarn run-prod` and open http://localhost:3001/

### `yarn run-prod`
Bundles react for production to the `build` folder and optimizes it. Then sets up the necessary database and starts the server and serves the front end files.
